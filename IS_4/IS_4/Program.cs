﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_4
{
    class Program
    {
        private static async Task<(Dictionary<string, float>, string[])> ComputeTf(FileInfo file, string outDirectoryPath)
        {
            var inStream = file.OpenRead();
            byte[] inputBytes = new byte[file.Length];
            await inStream.ReadAsync(inputBytes);

            var terms = Encoding.Unicode.GetString(inputBytes).Split('\n', StringSplitOptions.RemoveEmptyEntries)
                .Where(x => x.Length > 0).ToArray();

            var tf = new Dictionary<string, float>();
            foreach (var term in terms)
            {
                if (tf.ContainsKey(term))
                {
                    tf[term]++;
                }
                else
                {
                    tf.Add(term, 1);
                }
            }

            var keys = tf.Keys.ToArray();
            
            foreach (var key in keys)
            {
                tf[key] = MathF.Round(tf[key] / terms.Length, 5, MidpointRounding.AwayFromZero);
            }

            await File.WriteAllLinesAsync(outDirectoryPath + file.Name,
                tf.OrderBy(x => x.Key).Select(x =>
                    $"{x.Key}:{x.Value}"));
            return (tf, terms);
        }


        private static async Task ProcessFileWithTab(ConcurrentDictionary<string, float[]> tfidf,
            IReadOnlyDictionary<string, float> idf, FileInfo file, string outDirectoryPath)
        {
            var (tf, terms) = await ComputeTf(file, outDirectoryPath);
            int.TryParse(file.Name.Split('.').First(), out var fileIndex);

            foreach (var term in terms)
            {
                try
                {
                    tfidf.AddOrUpdate(term, new float[_fileNumber], (key, value) =>
                    {
                        value[fileIndex] = MathF.Round(tf[key] * idf[key], 5, MidpointRounding.AwayFromZero);
                        return value;
                    });
                }
                catch (KeyNotFoundException){ }
            }
        }


        private static async Task ProcessFileWithoutTab(IS_3.Program.InvIndex<float> tfidf,
            IReadOnlyDictionary<string, float> idf, FileInfo file,
            string outDirectoryPath)
        {
            var (tf, terms) = await ComputeTf(file, outDirectoryPath);
            int.TryParse(file.Name.Split('.').First(), out var fileIndex);

            foreach (var term in terms) 
            { 
                tfidf.Dict[term][fileIndex] = MathF.Round(tf[term] * idf[term], 5, MidpointRounding.AwayFromZero);
            }

        }


        private static void WriteToCsv<T1, T2>(string path, IEnumerable<KeyValuePair<T1, T2[]>> dictionary)
        {
            Console.WriteLine("Writing tf-idf...");
            using (var writer = new StreamWriter(path))
            {
                writer.Write("Name;");
                writer.WriteLine(string.Join(";", Enumerable.Range(0, _fileNumber)));

                foreach (var (key, values) in dictionary)
                {
                    writer.Write($"{key};");
                    writer.WriteLine(string.Join(";", values));
                }
            }
        }


        private static async Task Compute()
        {
            var lemmasDirectory = new DirectoryInfo(_pathToLemmasDirectory);
            var index = IS_3.Program.InvIndex<bool>.DeserializeIndex(_pathToIndexDirectory);
            var files = lemmasDirectory.GetFiles();
            _fileNumber = files.Length - 1;
            Console.WriteLine($"Processing: {_fileNumber} docs, {index.Dict.Count} words");

            var idf = new Dictionary<string, float>(index.Dict.Select(x =>
                new KeyValuePair<string, float>(x.Key,
                    MathF.Round(MathF.Log10((float) _fileNumber / x.Value.Count), 5))));
            await File.WriteAllLinesAsync(_pathToIdfDirectory + "idf.txt", idf.Select(x => $"{x.Key}:{x.Value}"));
            Console.WriteLine("idf was computed");

            if (_tabFlag)
            {
                var tfidfDict = new ConcurrentDictionary<string, float[]>();
                var tasks = files.Select(x => ProcessFileWithTab(tfidfDict, idf, x, _pathToTfDirectory)).ToArray();
                await Task.WhenAll(tasks);
                Console.WriteLine("tf and tf-idf were computed");
                WriteToCsv(_pathToIdfDirectory + "tf_idf.csv", tfidfDict.OrderBy(x => x.Key));
            }
            else
            {
                var newIndex = new IS_3.Program.InvIndex<float>();
                newIndex.Dict = new Dictionary<string, Dictionary<int, float>>(index.Dict.AsParallel().Select(pair =>
                    new KeyValuePair<string, Dictionary<int, float>>(pair.Key,
                        new Dictionary<int, float>(
                            pair.Value.Select(pair1 => new KeyValuePair<int, float>(pair1.Key, 0))))));
                var tasks = files.Select(x => ProcessFileWithoutTab(newIndex, idf, x, _pathToTfDirectory)).ToArray();
                await Task.WhenAll(tasks);
                Console.WriteLine("tf and tf-idf were computed");
                IS_3.Program.InvIndex<float>.Serialize(newIndex, _pathToIndexDirectory);
            }

            Console.WriteLine("Successfully written");
        }

        
        private static string _pathToCrawlerDirectory = "";
        private static string _pathToIndexDirectory, _pathToTfDirectory, _pathToIdfDirectory, _pathToLemmasDirectory;
        private static int _fileNumber;
        private static bool _tabFlag;


        static async Task Main()
        {
            Console.WriteLine("Enter the path to crawler's directory (last slash is required)");
            _pathToCrawlerDirectory = Console.ReadLine();
            try
            {
                _pathToIndexDirectory = $@"{_pathToCrawlerDirectory}InvIndex{_pathToCrawlerDirectory.Last()}";
                _pathToTfDirectory = $@"{_pathToCrawlerDirectory}tf{_pathToCrawlerDirectory.Last()}";
                _pathToIdfDirectory = $@"{_pathToCrawlerDirectory}idf{_pathToCrawlerDirectory.Last()}";
                _pathToLemmasDirectory = $@"/{_pathToCrawlerDirectory}Tokenizated{_pathToCrawlerDirectory.Last()}";
                Directory.CreateDirectory(_pathToIdfDirectory);
                Directory.CreateDirectory(_pathToTfDirectory);
                Console.WriteLine("Compute whole tf-idf tab?");
                while (!bool.TryParse(Console.ReadLine(),out _tabFlag))
                {
                    Console.WriteLine("Try again");
                }
                await Compute();
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
