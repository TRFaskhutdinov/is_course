﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IS_5
{
    class Program
    {
        static async Task<string> VectorSearch(string inp)
        {
            var query = inp.ToLowerInvariant().Split(' ').Select(x =>
                Regex.Replace(x, @"[^а-яА-Я]", "", RegexOptions.Compiled));
            ConcurrentDictionary<int, float> scored = new ConcurrentDictionary<int, float>();
            var tasks = query.Select(x => ProcessQueryItem(x, scored));
            await Task.WhenAll(tasks);
            return scored.IsEmpty
                ? ""
                : string.Join(" ",
                    scored.OrderByDescending(x => x.Value).Take(Math.Min(100, scored.Count)).Select(x => x.Key));
        }


        static Task ProcessQueryItem(string item, ConcurrentDictionary<int, float> scored)
        {
            try
            {
                foreach (var (doc, tfidf) in _invIndex.Dict[item])
                {
                    //scored.AddOrUpdate(doc, _tfidf[item][doc], (key, value) => value + _tfidf[item][key]);
                    scored.AddOrUpdate(doc, tfidf, (key, value) => value + tfidf);
                }
            }
            catch (KeyNotFoundException) { }

            return Task.CompletedTask;
        }


        static Dictionary<string, float[]> DeserializeTfidf(string path)
        {
            var list = new List<string>();
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    list.Add(reader.ReadLine());
                }
            }

            return new Dictionary<string, float[]>(list.Skip(1).Where(x => x.Length > 0).AsParallel()
                .Select(x => x.Split(';')).Select(x =>
                    new KeyValuePair<string, float[]>(x.First(), x.Skip(1).Select(float.Parse).ToArray())));
        }


        private static Dictionary<string, float[]> _tfidf;
        private static IS_3.Program.InvIndex<float> _invIndex;
        private static string _pathToIndexDirectory = "";
        private static string _pathToIdfDirectory = "";
        private static int _fileNumber;


        static async Task Main()
        {
            try
            {
                Console.WriteLine("Enter path to crawler's directory (last slash is reqiured)");
                var path = Console.ReadLine();
                _pathToIndexDirectory = $@"{path}InvIndex{path.Last()}";
                _pathToIdfDirectory = $@"{path}idf{path.Last()}";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }
            try
            {
                //_tfidf = DeserializeTfidf(PathToIdfDirectory + "tf_idf.csv");
                _invIndex = IS_3.Program.InvIndex<float>.DeserializeIndex(_pathToIndexDirectory);
                _fileNumber = _invIndex.MaxInd + 1;
            }
            catch (IOException) { }

            var f = true;

            while (f)
            {
                Console.WriteLine("Type search query or \"#exit\"");
                var inp = Console.ReadLine();

                switch (inp)
                {
                    case null:
                        continue;
                    case "":
                        continue;
                    case "#exit":
                        f = false;
                        break;
                    default:
                        try
                        {
                            var result = await VectorSearch(inp);
                            if (result.Length != 0)
                            {
                                Console.WriteLine($"100 the most relevant documents from {_fileNumber} docs: {result}");
                            }
                            else
                            {
                                Console.WriteLine("#         ###   #          #     #                 #######                             ");
                                Console.WriteLine("#    #   #   #  #    #     ##    #  ####  #####    #        ####  #    # #    # #####  ");
                                Console.WriteLine("#    #  #     # #    #     # #   # #    #   #      #       #    # #    # ##   # #    # ");
                                Console.WriteLine("#    #  #     # #    #     #  #  # #    #   #      #####   #    # #    # # #  # #    # ");
                                Console.WriteLine("####### #     # #######    #   # # #    #   #      #       #    # #    # #  # # #    # ");
                                Console.WriteLine("     #   #   #       #     #    ## #    #   #      #       #    # #    # #   ## #    # ");
                                Console.WriteLine("     #    ###        #     #     #  ####    #      #        ####   ####  #    # #####  ");
                            }
                            Console.WriteLine();
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Index file does not exist.");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        break;
                }
            }
        }
    }
}
