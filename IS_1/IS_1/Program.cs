﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abot2.Crawler;
using Abot2.Poco;
using AngleSharp;
using AngleSharp.Dom;

namespace IS_1
{
    class Program
    {
        private static StringBuilder _summary = new StringBuilder();
        private static int _counter;
        private static object lockObj = new object();
        private static string _path = "";
        private static string _url = "https://habr.com/ru/";
        private static int _pagesCount;
        private static CancellationTokenSource _tokenSource = new CancellationTokenSource();

        private static Func<PageToCrawl, CrawlContext, CrawlDecision> _habrOrientdedDecisionMaker =
            (pageToCrawl, crawlContext) =>
            {
                var decision = new CrawlDecision {Allow = true};
                if (!pageToCrawl.Uri.AbsoluteUri.Contains("https://habr.com")
                    || pageToCrawl.Uri.AbsoluteUri.Last() != '/'
                    || pageToCrawl.Uri.AbsoluteUri.Contains("news")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("top")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("flows")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("auth")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("compan") // company && companies
                    || pageToCrawl.Uri.AbsoluteUri.Contains("users")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("hub")
                    || pageToCrawl.Uri.AbsoluteUri.Contains("article"))
                    return new CrawlDecision {Allow = false, Reason = "crawls only habr, excludes uselessness"};

                return decision;
            };
        

        static async Task Main()
        {
            Console.WriteLine("Enter the target directory");
            _path = Console.ReadLine();
            try
            {
                Directory.CreateDirectory(_path);  // Last slash is required
                Console.WriteLine("Enter URL or skip");
                var input = Console.ReadLine();
                if (input.Length > 4)
                {
                    _url = input;
                }

                Console.WriteLine("Enter pages' count");
                while (!int.TryParse(Console.ReadLine(), out _pagesCount) && _pagesCount < 1)
                {
                    Console.WriteLine("Try again");
                }
                
                Crawler();

                var file = File.OpenWrite($@"{_path}index.txt");
                await file.WriteAsync(Encoding.Unicode.GetBytes(_summary.ToString()));
                file.Close();
                Console.WriteLine("index.txt was written.");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void Crawler()
        {
            var config = new CrawlConfiguration
            {
                MaxPagesToCrawl = 1000000,  // goes till _pagesCount
                MinCrawlDelayPerDomainMilliSeconds = 3000 //Wait this many millisecs between requests
            };
            var crawler = new PoliteWebCrawler(config);

            if (_url.Contains("https://habr.com"))
            {
                crawler.ShouldCrawlPageDecisionMaker = _habrOrientdedDecisionMaker;
            }

            //crawler.ShouldDownloadPageContentDecisionMaker = (crawledPage, crawlContext) =>
            //{
            //    var decision = new CrawlDecision {Allow = true};
            //    if (!crawledPage.Uri.AbsoluteUri.Contains(@"/post/"))
            //        return new CrawlDecision {Allow = false, Reason = "Only articles"};

            //    return decision;
            //};

            crawler.ShouldCrawlPageLinksDecisionMaker = (crawledPage, crawlContext) =>
            {
                var decision = new CrawlDecision { Allow = true };
                if (crawledPage.Content.Bytes.Length < 100)
                    return new CrawlDecision { Allow = false, Reason = "Just crawl links in pages that have at least 100 bytes" };

                return decision;
            };

            //crawler.PageCrawlStarting += Crawler_ProcessPageCrawlStarting;
            crawler.PageCrawlCompleted += Crawler_ProcessPageCrawlCompleted;
            //crawler.PageCrawlDisallowed += Crawler_PageCrawlDisallowed;
            //crawler.PageLinksCrawlDisallowed += Crawler_PageLinksCrawlDisallowed;

            try
            {
                //var crawlResult = await
                crawler.CrawlAsync(new Uri(_url), _tokenSource).Wait(_tokenSource.Token);
                    // Sadly, in this method cancellation token don't work with await(
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Crawling was executed.");
            }
            finally
            {
                _tokenSource.Dispose();
            }
        }


        private static void Crawler_ProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;

            if (crawledPage.HttpRequestException != null ||
                crawledPage.HttpResponseMessage.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine($"Crawl of page failed {crawledPage.Uri.AbsoluteUri}");
                return;
            }

            if (string.IsNullOrEmpty(crawledPage.Content.Text))
            {
                Console.WriteLine($"Page had no content {crawledPage.Uri.AbsoluteUri}");
                return;
            }

            if (_url.Contains("https://habr.com"))
            {
                if (!crawledPage.Uri.AbsoluteUri.Contains(@"/post/"))
                {
                    Console.WriteLine(
                        $"Crawl of page succeeded {crawledPage.Uri.AbsoluteUri}. Only links are collected");
                    return;
                }
            }

            Console.Write($"Crawl of page succeeded ({_counter + 1}/{_pagesCount}) {crawledPage.Uri.AbsoluteUri} ");


            var config = Configuration.Default;
            var context = BrowsingContext.New(config);
            var doc = context.OpenAsync(req => req.Content(crawledPage.Content.Text));
            doc.Wait();
            string postText="";
            if(_url.Contains("https://habr.com"))
            {
                var selects = doc.Result.QuerySelectorAll("div");
                try
                {
                    postText = selects.GetElementById("post-content-body").TextContent + selects.GetElementById("comments").TextContent;

                }
                catch (Exception exception)
                {
                    Console.WriteLine($" Parsing: {exception.Message}. ");
                }
            }
            else
            {
                postText = doc.Result.TextContent;
                if (postText == null)
                {
                    var childrens = doc.Result.Children.Where(x => x.TextContent != null).ToList();
                    var chosenOne = childrens.FirstOrDefault(x =>
                        x.TextContent.Length == childrens.Max(x1 => x1.TextContent.Length));
                    if (chosenOne != null)
                    {
                        postText = chosenOne.TextContent;
                    }
                }
            }

            try
            {
                if (postText is null)
                {
                    Console.WriteLine("Empty inner!");
                    return;
                }

                if (postText.Length < 10000)
                {
                    Console.WriteLine("Not enough words!");
                    return;
                }
                Console.WriteLine();

                int tempCounter;
                lock (lockObj)
                {
                    tempCounter = _counter++;
                    _summary.AppendLine($"{tempCounter} {e.CrawledPage.Uri.AbsoluteUri}");
                    if (_counter >= _pagesCount)
                    {
                        _tokenSource.Cancel(false);
                    }
                }

                var file = File.OpenWrite($@"{_path}{tempCounter}.txt");
                file.Write(Encoding.Unicode.GetBytes(postText));
                file.Close();

            }
            catch (Exception exception)
            {
                Console.WriteLine($" Parsing: {exception.Message}. ");
            }
        }

        #region Unused

        //private static async Task PageRequest()
        //{
        //    var pageRequester = new PageRequester(new CrawlConfiguration(), new WebContentExtractor());

        //    var crawledPage = await pageRequester.MakeRequestAsync(new Uri("http://google.com"));

        //    lock (lockObj)
        //    {
        //        summary.AppendLine($"{counter++} {crawledPage.Uri}");
        //    }


        //    Log.Logger.Information("{result}", new
        //    {
        //        url = crawledPage.Uri,
        //        status = Convert.ToInt32(crawledPage.HttpResponseMessage.StatusCode)
        //    });
        //}

        private static void PageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            var httpStatus = e.CrawledPage.HttpResponseMessage.StatusCode;
            var rawPageText = e.CrawledPage.Content.Text;

        }

        private static void Crawler_ProcessPageCrawlStarting(object sender, PageCrawlStartingArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            Console.WriteLine($"About to crawl link {pageToCrawl.Uri.AbsoluteUri} which was found on page {pageToCrawl.ParentUri.AbsoluteUri}");
        }


        private static void Crawler_PageLinksCrawlDisallowed(object sender, PageLinksCrawlDisallowedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;
            Console.WriteLine($"Did not crawl the links on page {crawledPage.Uri.AbsoluteUri} due to {e.DisallowedReason}");
        }

        private static void Crawler_PageCrawlDisallowed(object sender, PageCrawlDisallowedArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            Console.WriteLine($"Did not crawl page {pageToCrawl.Uri.AbsoluteUri} due to {e.DisallowedReason}");
        }


        //var file = File.OpenRead($@"{_path}0.txt");
        //byte[] barr = new byte[file.Length];
        //await file.ReadAsync(barr);
        //file.Close();
        //var config = Configuration.Default;
        //var context = BrowsingContext.New(config);
        //var doc = await context.OpenAsync(req => req.Content(Encoding.Unicode.GetString(barr)));
        //var postText = doc.QuerySelectorAll("div").GetElementById("post-content-body");
        //file = File.OpenWrite($@"{_path}index.txt");
        //await file.WriteAsync(Encoding.Unicode.GetBytes(postText.OuterHtml));
        //file.Close();
        #endregion
    }
}
