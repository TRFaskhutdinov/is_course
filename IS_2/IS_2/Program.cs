﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DeepMorphy;

namespace IS_2
{
    class Program
    {
        private static string _path = "";

        static void Main()
        {
            MorphAnalyzer analyzer = new MorphAnalyzer(true);
            try
            {
                Console.WriteLine("Enter path to crawler's directory (last slash is required)");
                _path = Console.ReadLine();
                DirectoryInfo dr = new DirectoryInfo(_path);  // Last slash is required
                var files = dr.GetFiles();
                Directory.CreateDirectory(_path + "Tokenizated");
            
                foreach (var file in files)
                {
                    try
                    {
                        Console.WriteLine($"Processing {file.Name}");
                        var inStream = file.OpenRead();
                        byte[] inputBytes = new byte[file.Length];
                        inStream.Read(inputBytes);
                        var content = Encoding.Unicode.GetString(inputBytes);

                        var tokens = Regex.Split(content, @"[^а-яА-Яa-zA-Z0-9_]", RegexOptions.Compiled)
                            .Select(x => Regex.Replace(x, @"[^а-яА-Я]", "", RegexOptions.Compiled))
                            .Where(x => x.Length > 0);
                        
                        var res = string.Join("\n", analyzer.Parse(tokens).Select(x => x.BestTag.Lemma));

                        var outStream = File.OpenWrite($@"{_path}Tokenizated{_path.Last()}{file.Name}");
                        outStream.Write(Encoding.Unicode.GetBytes(res));
                        outStream.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"{e.Message} was occured.");
                    }
                }

                Console.WriteLine("Owatta!");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
