﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IS_3
{
    public class Program
    {
        [Serializable]
        public class InvIndex<T>
        {
            public Dictionary<string, Dictionary<int, T>> Dict { get; set; }
            
            public int MaxInd;

            [NonSerialized]
            private static BinaryFormatter _serializer = new BinaryFormatter();

            public static void Serialize(InvIndex<T> index, string path)
            {
                using (var fs = new FileStream($"{path}index_{typeof(T)}.dat", FileMode.Create))
                {
                    _serializer.Serialize(fs, index);
                }
            }

            private HashSet<int> TryGetValue(string key)
            {
                return Dict.TryGetValue(key, out var value) ? new HashSet<int>(value.Keys) : new HashSet<int>();
            }

            public static InvIndex<T> DeserializeIndex(string path)
            {
                using (var fs = new FileStream($"{path}index_{typeof(T)}.dat", FileMode.Open))
                {
                    return (InvIndex<T>) _serializer.Deserialize(fs);
                }
            }

            public InvIndex(ConcurrentDictionary<string, Dictionary<int, T>> dictionary, int maxInd)
            {
                Dict = new Dictionary<string, Dictionary<int, T>>(dictionary);
                MaxInd = maxInd;
                Serialize(this, _pathToIndexDirectory);
                var sumString = string.Join("\n", Dict.OrderBy(x=>x.Key).Select(keyValue =>
                    $"{keyValue.Key}: {string.Join(" ", keyValue.Value.Keys)}"));

                File.WriteAllText($@"{_pathToIndexDirectory}index.txt", sumString, Encoding.Unicode);
            }

            public InvIndex() { }

            public static IEnumerable<string> BooleanSearch(string input, InvIndex<T> index)
            {
                var parsedInput = Regex.Split(input.ToLowerInvariant(), @" \| ", RegexOptions.Compiled)
                    .Select(x => Regex.Split(x, " & ", RegexOptions.Compiled));
                var rawResult = parsedInput
                        .Select(x => x.Select(y =>
                        {
                            if (!y.StartsWith('!')) return index.TryGetValue(y);
                            var tempSet = new HashSet<int>(Enumerable.Range(0, index.MaxInd + 1));
                            tempSet.ExceptWith(index.TryGetValue(y.Substring(1)));
                            return tempSet;

                        }).Aggregate((s, k) =>
                        {
                            s.IntersectWith(k);
                            return s;
                        })).Aggregate((s, k) =>
                        {
                            s.UnionWith(k);
                            return s;
                        });

                if (rawResult.Count == 0)
                {
                    Console.WriteLine(@"                              _____                        _____                                      ");
                    Console.WriteLine(@"  __   _  _____  __   _   ___|   _ |__  _____    __     __|___  |__  _____  __   _  ____   _  _____   ");
                    Console.WriteLine(@" |  | | |/     ||  | | | |    \ | |   |/     \ _|  |_  |   ___|    |/     \|  | | ||    \ | ||     \  ");
                    Console.WriteLine(@" |  |_| ||  /  ||  |_| | |     \| |   ||     ||_    _| |   ___|    ||     ||  |_| ||     \| ||      \ ");
                    Console.WriteLine(@" '----__||_____/'----__| |__/\____| __|\_____/  |__|   |___|     __|\_____/|______||__/\____||______/ ");
                    Console.WriteLine(@"                             |_____|                      |_____|                                     ");
                    Console.WriteLine(@"                                                                                                      ");
                    return new List<string> {""};
                }

                return rawResult.Select(x=>x.ToString());
            }
        }

        private static async Task ProcessFile(ConcurrentDictionary<string, Dictionary<int, bool>> tokenDict, FileInfo file)
        {
            var inStream = file.OpenRead();
            byte[] inputBytes = new byte[file.Length];
            await inStream.ReadAsync(inputBytes);

            var lemmas = Encoding.Unicode.GetString(inputBytes).Split('\n').Distinct().Where(x => x.Length > 0);
            if (!int.TryParse(file.Name.Split('.').First(), out var fileNum))
                return;

            foreach (var lemma in lemmas)
            {
                tokenDict.AddOrUpdate(lemma, new Dictionary<int, bool>() {{fileNum, false}}, (key, value) => 
                {
                    value.Add(fileNum, false);
                    return value;
                });
            }
        }

        private static async Task CreateIndex()
        {
            DirectoryInfo dr = new DirectoryInfo(_path + $"Tokenizated{_path.Last()}");
            Directory.CreateDirectory(_pathToIndexDirectory);
            ConcurrentDictionary<string, Dictionary<int, bool>> tokenDict = new ConcurrentDictionary<string, Dictionary<int, bool>>();
            var tasks = dr.GetFiles().Select(x => ProcessFile(tokenDict, x)).ToArray();
            await Task.WhenAll(tasks);

            _index = new InvIndex<bool>(tokenDict,
                dr.GetFiles().Select(x => x.Name.Split('.').First()).Where(x => x.All(char.IsNumber))
                    .Select(x => int.Parse(x)).Max());
            Console.WriteLine("Index was created");
        }

        
        private static string _path = "";
        private static string _pathToIndexDirectory = "";
        private static InvIndex<bool> _index;
        static async Task Main()
        {
            try
            {
                _index = InvIndex<bool>.DeserializeIndex(_pathToIndexDirectory);
            }
            catch (IOException) { }
            var f = true;
            try
            {
                Console.WriteLine("Enter path to crawler's directory (last slash is required)");
                _path = Console.ReadLine();
                _pathToIndexDirectory = $@"{_path}InvIndex{_path.Last()}";
                while (f)
                {
                    Console.WriteLine("Type search query or \"#create\" to create inverted index or \"#exit\"");
                    var inp = Console.ReadLine();
                    switch (inp)
                    {
                        case "":
                            continue;
                        case "#create":
                            try
                            {
                                var task = CreateIndex();
                                await task;
                                task.Dispose();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                            break;
                        case "#exit":
                            f = false;
                            break;
                        default:
                            var result = InvIndex<bool>.BooleanSearch(inp, _index).ToArray();
                            if (result.Length > 20) 
                            {
                                Console.Write(string.Join(" ", result.Take(Math.Min(20, result.Length))));
                                Console.WriteLine($"... and {result.Length - 20} more.");
                            }
                            else
                            {
                                Console.WriteLine(string.Join(" ", result));
                            }
                            Console.WriteLine();
                            break;
                    }
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Index file does not exist.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
